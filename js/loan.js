const values =  {
  currency: '₦',
  loanAmount: '',
  tenure: 5,
  rate: 10,
  period: 'Yr',
}

const tenureConverter = {
  'M': { MAX: 240, STEP: 60, width: '17px' },
  'Yr': { MAX: 20, STEP: 5, width: '10.5px' }
}

let isLoading = false

const currencyInput = document.querySelector('select[name="currency"]');
const inputs = document.querySelectorAll('input');
const form = document.querySelector('form');
const currencySymbolElems = document.querySelectorAll('.currency_symbol');
const rateElems = document.querySelectorAll('input[name="rate"]');
const tenureElems = document.querySelectorAll('input[name="tenure"]');
const tenurePeriodElems = document.querySelectorAll('.tenure_period');
const piechart = document.querySelector('#piechart');
const monthlyPaymentElement = document.querySelector('#monthlyPayment');
const totalPrincipalElement = document.querySelector('#totalPrincipal');
const totalInterestElement = document.querySelector('#totalInterest');
const chartContainer = document.querySelector('#chart-container');
const loaderContainer = document.querySelector('#loader-container');
const tenurePeriodScale = document.querySelector('#tenure_period_scale');
const tenureErrElem = document.querySelector('#tenure_err');
const loanErrElem = document.querySelector('#loan_err');
const rateErrElem = document.querySelector('#rate_err');

const formatCurrency = (value) => value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });

const getCanvas = () => {
  const canvas = document.createElement('canvas');
  canvas.style.width = '100%';
  canvas.style.maxWidth = '200px';
  canvas.style.maxHeight = '240px';
  return canvas;
}

const validateLoanAmount = () => {
  const minCost = 0;
  const maxCost = 1000000000;
  const loanAmount = values.loanAmount;
  const isValid = loanAmount && (loanAmount > minCost && loanAmount <= maxCost);
  const message = isValid ? `` : `Loan amount should be greater than 0 and not more than 1B`;
  loanErrElem.textContent = message

  return isValid
}

const validateInterestRate = () => {
  const minRate = 0;
  const maxRate = 100;
  const rate = values.rate;
  const isValid = rate >= minRate && rate <= maxRate;
  const message = isValid ? `` : `Interest rate should be between ${minRate} and ${maxRate}`;
  rateErrElem.textContent = message;

  return isValid;
}

const validateTenure = () => {
  const { period, tenure } = values;
  const isValid = tenure >= 1 && tenure <= tenureConverter[period].MAX;
  const message =  isValid ? `` : `Tenure should be between 1 month and 20 years`;
  tenureErrElem.textContent = message

  return isValid;
}

const setRateValue = (value) => {
  rateElems.forEach(element => element.value = value || 0)
  validateInterestRate();
}

const setTenureValue = (value) => {
  tenureElems.forEach(element => element.value = value || 0)
  validateTenure();
}

const selectTarget = (target) => {
  tenurePeriodElems.forEach(input => input.parentElement.classList.remove('selected'));
  target.parentElement.classList.add('selected');
  tenurePeriodScale.innerHTML = ''
  const { MAX, STEP } = tenureConverter[target.value];
  const numberOfElements = MAX / STEP
  Array.from({ length: numberOfElements + 1 }).forEach((_, index) => {
    const element = document.createElement('li');
    element.style.width = tenureConverter[target.value].width;
    element.textContent = index * STEP;
    tenurePeriodScale.appendChild(element)
  })
  tenureElems.forEach(element => {
    element.setAttribute('max', MAX);
    element.value = values.tenure;
  })
  validateTenure()
}

const displayChart = ({ totalInterestPaid, totalPrinciplePaid, monthlyPayment }) => {
  monthlyPaymentElement.textContent = `${values.currency} ${formatCurrency(monthlyPayment)}`
  totalPrincipalElement.textContent = `${values.currency} ${formatCurrency(totalPrinciplePaid)}`
  totalInterestElement.textContent = `${values.currency} ${formatCurrency(totalInterestPaid)}`

  const xValues = ["Total principal", "Total interest"];
  const yValues = [totalPrinciplePaid, totalInterestPaid,];
  const barColors = ["#ED1C24", "#808285"];

  const canvas = getCanvas()
  loaderContainer.classList.add('hide');
  chartContainer.classList.remove('hide');
  piechart.replaceChildren(canvas);
  
  new Chart(canvas.getContext('2d'), {
    type: "pie",
    data: {
      labels: xValues,
      datasets: [{
        backgroundColor: barColors,
        data: yValues
      }]
    },
    options: {
      plugins: {
        legend: {
          position: 'bottom',
          align: 'start',
          labels: { usePointStyle: true }
        }
      },
    }
  });
}

form.addEventListener('submit', (e) => {
  e.preventDefault();
  calculateBusinessLoan();
})

inputs.forEach(input => input.addEventListener('change', ({ target }) => {
  let convertedValue = target.value;
  if (target.name === 'loanAmount') {
    convertedValue = parseFloat(target.value.replace(/[,\s+]/g, ''))
    if (!Number.isNaN(convertedValue)) {
      target.value = formatCurrency(convertedValue);
    }
    values[target.name] = convertedValue;
 
    validateLoanAmount();
  } else {
    values[target.name] = convertedValue;
    if (target.name === 'rate') setRateValue(target.value)
    else if (target.name === 'tenure') setTenureValue(target.value)
    else if (target.name === 'period') selectTarget(target)
  }
}));

currencyInput.addEventListener('change', ({ target }) => {
  values.currency = target.value;
  currencySymbolElems.forEach(element => element.textContent = target.value)
});

const calculateBusinessLoan = async () => {
  const isValidInputs = !isLoading && validateLoanAmount() && validateTenure() && validateInterestRate();
  if(!isValidInputs) return;

  isLoading = true;
  chartContainer.classList.add('hide');
  loaderContainer.classList.remove('hide');
  const { BASE_URL, TOKEN } = await (await fetch("./config.json")).json();
  let { rate, loanAmount, tenure, period } = values;
  tenure = period === 'M' ? tenure / 12 : tenure
  try {
    const response = await fetch(
      `${BASE_URL}/calculate/REGULAR?loanAmount=${loanAmount}&loanTenure=${tenure}&interestRate=${rate}`,
      {
        headers: { Authorization: TOKEN },
      }
    );
    displayChart(await response.json());
  } catch (error) {
    console.log(error);
  } finally {
    isLoading = false;
  }
};
