const values =  {
  currency: '₦',
  cost: '',
  payment: '',
  amount: '',
  tenure: 10,
  rate: 10,
  period: 'Yr',
}

const tenureConverter = {
  'M': { MAX: 180, STEP: 36, width: '17px' },
  'Yr': { MAX: 15, STEP: 3, width: '10.5px' }
}

let isLoading = false

const currencyInput = document.querySelector('select[name="currency"]');
const inputs = document.querySelectorAll('input');
const form = document.querySelector('form');
const currencySymbolElems = document.querySelectorAll('.currency_symbol');
const rateElems = document.querySelectorAll('input[name="rate"]');
const tenureElems = document.querySelectorAll('input[name="tenure"]');
const loanAmountElem = document.querySelector('div[name="loanAmount"]');
const tenurePeriodElems = document.querySelectorAll('.tenure_period');
const paymentPercentage = document.querySelector('.payment_percentage');
const piechart = document.querySelector('#piechart');
const monthlyPaymentElement = document.querySelector('#monthlyPayment');
const totalPrincipalElement = document.querySelector('#totalPrincipal');
const totalInterestElement = document.querySelector('#totalInterest');
const chartContainer = document.querySelector('#chart-container');
const loaderContainer = document.querySelector('#loader-container');
const tenurePeriodScale = document.querySelector('#tenure_period_scale');
const paymentErrElem = document.querySelector('#err_payment');
const tenureErrElem = document.querySelector('#err_tenure');
const costErrElem = document.querySelector('#err_cost');
const rateErrElem = document.querySelector('#err_rate');
const loanAmountErrElem = document.querySelector('#err_loan_amount');

const formatCurrency = (value) => value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });

const getCanvas = () => {
  const canvas = document.createElement('canvas');
  canvas.style.width = '100%';
  canvas.style.maxWidth = '200px';
  canvas.style.maxHeight = '240px';
  return canvas;
}

const validateCost = () => {
  const min = 0;
  const cost = values.cost;
  const isValid = cost && (cost > min);
  const message = isValid ? `` : `Cost of asset should be greater than 0`;
  costErrElem.textContent = message

  return isValid
}

const validateDownPayment = () => {
  const { cost, payment } = values;
  const isValid = payment && (payment >= 0 && payment < cost);
  const message = isValid ? '' : `Equity contribution should be positive and less than the cost of asset`;
  paymentErrElem.textContent = message;

  return isValid;
}

const validateLoanAmount = () => {
  const min = 0;
  const maxRate = 1000000000;
  const amount = values.amount;
  const isValid = amount && (amount > min && amount <= maxRate);
  const message = isValid ? `` : `Loan amount should be greater than 0 and not more than 1B, adjust cost of asset and/or equity contribution`;
  loanAmountErrElem.textContent = message

  return isValid
}

const validateInterestRate = () => {
  const minRate = 0;
  const maxRate = 100;
  const rate = values.rate;
  const isValid = rate >= minRate && rate <= maxRate;
  const message = isValid ? `` : `Interest rate should be between ${minRate} and ${maxRate}`;
  rateErrElem.textContent = message;

  return isValid;
}

const validateTenure = () => {
  const { period, tenure } = values;
  const isValid = tenure >= 1 && tenure <= tenureConverter[period].MAX;
  const message =  isValid ? `` : `Tenure should be between 1 month and 15 years`;
  tenureErrElem.textContent = message

  return isValid;
}

const setDownPaymentPercentage = () => {
  const { payment, cost } = values;
  let percent = 0;
  let loanAmount = ''
  const hasValidValues = cost !== '' && validateCost() && payment && validateDownPayment();
  if (hasValidValues) {
    values.amount = cost - payment
    percent = (payment * 100 / cost).toFixed(1);
    loanAmount = formatCurrency(values.amount);
    validateLoanAmount();
  }
  loanAmountElem.textContent = loanAmount
  paymentPercentage.textContent = `${percent}%`;
}

const setRateValue = (value) => {
  rateElems.forEach(element => element.value = value || 0)
  validateInterestRate();
}

const setTenureValue = (value) => {
  tenureElems.forEach(element => element.value = value || 0)
  validateTenure();
}

const selectTarget = (target) => {
  tenurePeriodElems.forEach(input => input.parentElement.classList.remove('selected'));
  target.parentElement.classList.add('selected');
  tenurePeriodScale.innerHTML = ''
  const { MAX, STEP } = tenureConverter[target.value];
  const numberOfElements = MAX / STEP
  Array.from({ length: numberOfElements + 1 }).forEach((_, index) => {
    const element = document.createElement('li');
    element.style.width = tenureConverter[target.value].width;
    element.textContent = index * STEP;
    tenurePeriodScale.appendChild(element)
  })
  tenureElems.forEach(element => {
    element.setAttribute('max', MAX);
    element.value = values.tenure;
  })
  validateTenure()
}

const displayChart = ({ totalInterestPaid, totalPrinciplePaid, monthlyPayment }) => {
  monthlyPaymentElement.textContent = `${values.currency} ${formatCurrency(monthlyPayment)}`
  totalPrincipalElement.textContent = `${values.currency} ${formatCurrency(totalPrinciplePaid)}`
  totalInterestElement.textContent = `${values.currency} ${formatCurrency(totalInterestPaid)}`

  const xValues = ["Total principal", "Total interest"];
  const yValues = [totalPrinciplePaid, totalInterestPaid,];
  const barColors = ["#ED1C24", "#808285"];

  const canvas = getCanvas()
  loaderContainer.classList.add('hide');
  chartContainer.classList.remove('hide');
  piechart.replaceChildren(canvas);
  
  new Chart(canvas.getContext('2d'), {
    type: "pie",
    data: {
      labels: xValues,
      datasets: [{
        backgroundColor: barColors,
        data: yValues
      }]
    },
    options: {
      plugins: {
        legend: {
          position: 'bottom',
          align: 'start',
          labels: { usePointStyle: true }
        }
      },
    }
  });
}

form.addEventListener('submit', (e) => {
  e.preventDefault();
  calculateMortgage();
})

inputs.forEach(input => input.addEventListener('change', ({ target }) => {
  let convertedValue = target.value;
  if (target.name === 'cost' || target.name === 'payment') {
    convertedValue = parseFloat(target.value.replace(/[,\s+]/g, ''))
    if (!Number.isNaN(convertedValue)) {
      target.value = formatCurrency(convertedValue);
    }
    values[target.name] = convertedValue;
 
    setDownPaymentPercentage()
  } else {
    values[target.name] = convertedValue;
    if (target.name === 'rate') setRateValue(target.value)
    else if (target.name === 'tenure') setTenureValue(target.value)
    else if (target.name === 'period') selectTarget(target)
  }
}));

currencyInput.addEventListener('change', ({ target }) => {
  values.currency = target.value;
  currencySymbolElems.forEach(element => element.textContent = target.value)
});

const calculateMortgage = async () => {
  const isValidInputs = !isLoading && validateCost() && validateDownPayment() && validateLoanAmount() && validateTenure() && validateInterestRate();
  if(!isValidInputs) return;

  isLoading = true;
  chartContainer.classList.add('hide');
  loaderContainer.classList.remove('hide');
  const { BASE_URL, TOKEN } = await (await fetch("./config.json")).json();
  let { rate, amount, tenure, period } = values;
  tenure = period === 'M' ? tenure / 12 : tenure
  try {
    const response = await fetch(
      `${BASE_URL}/calculate/MORTGAGE?loanAmount=${amount}&loanTenure=${tenure}&interestRate=${rate}`,
      {
        headers: { Authorization: TOKEN },
      }
    );
    displayChart(await response.json());
  } catch (error) {
    console.log(error);
  } finally {
    isLoading = false;
  }
};
