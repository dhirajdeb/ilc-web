const values =  {
  currency: '₦',
  currentAge: '',
  retireAge: '',
  expectancyAge: '',
  income: '',
  monthSavings: '',
  monthSavingValue: '',
  monthExpenditure: '',
  totalSavings: '',
}

let isLoading = false

const currencyInput = document.querySelector('select[name="currency"]');
const inputs = document.querySelectorAll('input');
const form = document.querySelector('form');
const currencySymbolElems = document.querySelectorAll('.currency_symbol');
const graphchart = document.querySelector('#graphchart');
const totalRetirementSavingsElement = document.querySelector('#totalRetirementSavings');
const timeToExhaustRetirementElement = document.querySelector('#timeToExhaustRetirementSavings');
const lifeExpectancyElement = document.querySelector('#lifeExpectancy');
const monthlySavingsRetirementElement = document.querySelector('#monthlySavingsToMeetRetirementNeeds');
const fundsRequiredElement = document.querySelector('#fundsRequiredToMeetCurrentSpending');
const annualSpentRetirementElement = document.querySelector('#annualSpentAfterRetirement');
const chartContainer = document.querySelector('#graph-container');
const loaderContainer = document.querySelector('#loader-container');
const currentAgeErrElem = document.querySelector('#current_age_err');
const retireAgeErrElem = document.querySelector('#retire_age_err');
const incomeErrElem = document.querySelector('#income_err');
const savingsErrElem = document.querySelector('#month_saving_err');
const totalSavingsErrElem = document.querySelector('#total_saving_err');
const lifeElem = document.querySelector('#expectancy_age_err');
const monthSavingValueElem = document.querySelector('#monthSavingValue');
const monthExpenditureValueElem = document.querySelector('#monthExpenditure');

const formatCurrency = (value) => value.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });

const getCanvas = () => {
  const canvas = document.createElement('canvas');
  canvas.style.width = '100%';
  canvas.style.maxWidth = '500px';
  canvas.style.maxHeight = '340px';
  canvas.style.height = '300px';
  return canvas;
}

const validateIncome = () => {
  const isValid = values.income > 0;
  const message = isValid ? `` : `Income should be greater than 0`;
  incomeErrElem.textContent = message

  return isValid
}

const validateTotalSavingsIncome = () => {
  const isValid = values.totalSavings > 0;
  const message = isValid ? `` : `Total savings should be greater than 0`;
  totalSavingsErrElem.textContent = message

  return isValid
}

const validateMonthSavings = () => {
  const min = 0;
  const max = 100;
  const monthSavings = values.monthSavings;
  const isValid = monthSavings >= min && monthSavings <= max;
  const message = isValid ? `` : `Monthly savings should be between ${min} and ${max}%`;
  savingsErrElem.textContent = message;

  return isValid;
}

const validateLife = () => {
  const expectancyAge = values.expectancyAge;
  const min = values.retireAge;
  const max = 80;
  const isValid = expectancyAge > min && expectancyAge <= max;
  const message = isValid ? `` : `Life expectancy should be greater than the retirement age and not more than ${max} years`;
  lifeElem.textContent = message;

  return isValid;
}

const validateCurrentAge = () => {
  const { currentAge } = values;
  const isValid = currentAge >= 18
  const message =  isValid ? `` : `Current age should be greater than or equal to 18`;
  currentAgeErrElem.textContent = message

  return isValid;
}

const validateRetireAge = () => {
  const { retireAge, currentAge } = values;
  const isValid = retireAge > currentAge;
  const message =  isValid ? `` : `Retirement age should be greater than the current age`;
  retireAgeErrElem.textContent = message

  return isValid;
}

const validateAge = () => validateCurrentAge() && values.retireAge && validateRetireAge() && values.expectancyAge && validateLife();

const setMonthlyValue = () => {
  const { income, monthSavings } =  values;
  let monthSavingValue = '';
  let monthExpenditureValue = '';
  const hasValidValues = income !== '' && validateIncome() && monthSavings !== '' && validateMonthSavings();
  if (hasValidValues) {
    const monthlyIncome = income / 12;
    monthSavingValue = monthlyIncome * monthSavings / 100;
    monthExpenditureValue = monthlyIncome - monthSavingValue;
  }
  monthSavingValueElem.textContent = formatCurrency(monthSavingValue);
  monthExpenditureValueElem.textContent = formatCurrency(monthExpenditureValue);
}

const formatNumber = (num) => {
  if (num >= 1000000000) {
    return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'B';
  }
  if (num >= 1000000) {
    return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
  }
  if (num >= 1000) {
    return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
  }
  return num;
};

const displayChart = (data) => {
  totalRetirementSavingsElement.textContent = `${values.currency} ${formatCurrency(data.totalSavingsAfterRetirement)}`
  timeToExhaustRetirementElement.textContent = `${data.timeToExhaustRetirementSavings} years`
  lifeExpectancyElement.textContent = `age ${values.expectancyAge}`
  fundsRequiredElement.textContent = `${values.currency} ${formatCurrency(data.fundsRequiredToMeetCurrentSpending)}`
  annualSpentRetirementElement.textContent = `${values.currency} ${formatCurrency(data.annualSpentAfterRetirement)}`
  monthlySavingsRetirementElement.textContent = `${values.currency} ${formatCurrency(data.monthlySavingsToMeetRetirementNeeds)} per month`

  const canvas = getCanvas()
  loaderContainer.classList.add('hide');
  chartContainer.classList.remove('hide');
  graphchart.replaceWith(canvas);

  const xValues = [...new Set([...Object.keys(data.spending), ...Object.keys(data.savingReservoir)])].sort((a, b) => a - b);
  const yValues = Object.values(data.savingReservoir)
  const yValues2 = Object.values(data.spending);

  new Chart(canvas.getContext('2d'), {
    type: "line",
    data: {
      labels: xValues,
      datasets: [
        {
          fill: false,
          label: 'Projected nest egg (funds in reservoir)',
          lineTension: 0,
          backgroundColor: "#ED1C24",
          borderColor: "rgba(237, 28, 36, 0.2)",
          data: yValues

        },
        {
          fill: false,
          label: 'Projected spending in retirement',
          lineTension: 0,
          backgroundColor: "#808285",
          borderColor: "rgba(128, 130, 133, 0.2)",
          data: yValues2
        },
      ]
    },
    options: {
      plugins: {
        legend: {
          position: 'bottom',
          align: 'start',
          labels: { usePointStyle: true }
        }
      },
      scales: {
        y: {
          ticks: {
            callback: formatNumber
          },
        },
      },
    }
  });
}

form.addEventListener('submit', (e) => {
  e.preventDefault();
  calculateRetirement();
})

inputs.forEach(input => input.addEventListener('change', ({ target }) => {
  let convertedValue = target.value;
  if (target.name === 'income' || target.name === 'totalSavings') {
    convertedValue = parseFloat(target.value.replace(/[,\s+]/g, ''))
    if (!Number.isNaN(convertedValue)) {
      target.value = formatCurrency(convertedValue);
    }
    values[target.name] = convertedValue;
    if (target.name === 'totalSavings') validateTotalSavingsIncome();
    if (target.name === 'income') setMonthlyValue();
  } else {
    values[target.name] = parseFloat(convertedValue);
    if (target.name === 'currentAge' || target.name === 'retireAge') validateAge()
    else if (target.name === 'monthSavings') setMonthlyValue()
    else if (target.name === 'expectancyAge') validateCurrentAge() && validateRetireAge() && validateLife()
  }
}));

currencyInput.addEventListener('change', ({ target }) => {
  values.currency = target.value;
  currencySymbolElems.forEach(element => element.textContent = target.value)
});

const calculateRetirement = async () => {
  const isValidInputs = !isLoading && validateCurrentAge() && validateRetireAge() && validateLife() && validateIncome() && validateMonthSavings() && validateTotalSavingsIncome();
  if(!isValidInputs) return;

  isLoading = true;
  chartContainer.classList.add('hide');
  loaderContainer.classList.remove('hide');
  const { BASE_URL, TOKEN } = await (await fetch("./config.json")).json();
  const { income, retireAge, currentAge, monthSavings, totalSavings } = values;

  try {
    const response = await fetch(
      `${BASE_URL}/calculate/RETIREMENT?netIncome=${income}&retirementAge=${retireAge}&currentAge=${currentAge}&monthlySavings=${monthSavings}&currentRetirementSavings=${totalSavings}`,
      {
        headers: { Authorization: TOKEN },
      }
    );
    displayChart(await response.json());
  } catch (error) {
    console.log(error);
  } finally {
    isLoading = false;
  }
};
